from django.urls import path
from .views import list_appointments, appointment_detail, technician_detail, list_technicians, cancel_appointment, finish_appointment

urlpatterns = [
    path("appointments/", list_appointments, name="list_appointments"),
    path("appointments/<int:pk>/", appointment_detail, name="appointment_detail"),
    path("technicians/", list_technicians, name="list_technicians"),
    path("technicians/<int:pk>/", technician_detail, name="technician_detail"),
    path("appointments/<int:pk>/cancel/", cancel_appointment, name="cancel_appointment"),
    path("appointments/<int:pk>/finish/", finish_appointment, name="finish_appointment"),
]
