from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin




class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.first_name + " " + self.last_name

    class Meta:
        ordering = ("id", "employee_id", "first_name")


class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.CharField(max_length=250)
    phone_number = models.CharField(max_length=15, unique=True)

    def __str__(self):
        return self.first_name + " " + self.last_name


    class Meta:
        ordering = ("id", "first_name", "last_name", "address", "phone_number")


class Sale(models.Model):
    price = models.CharField(max_length=20)

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name = "automobile",
        on_delete =models.CASCADE,
    )

    salesperson = models.ForeignKey(
        Salesperson,
        related_name= "salesperson",
        on_delete= models.CASCADE,
    )

    customer = models.ForeignKey(\
        Customer,
        related_name = "customer",
        on_delete = models.CASCADE,
        )

    class Meta:
        ordering = ("id", "price")
