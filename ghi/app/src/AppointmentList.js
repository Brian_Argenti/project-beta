import { useState, useEffect } from "react";

const AppointmentsList = () => {
	const [appointments, setAppointments] = useState([]);

	useEffect(() => {
		const fetchAppointments = async () => {
			const url = "http://localhost:8080/api/appointments/";
			const response = await fetch(url);

			if (response.ok) {
				const data = await response.json();
				setAppointments(data.appointments);
			}
		};
		fetchAppointments();

	}, []);

	const finishAppointment = async (id) => {
		const appointmentUrl = `http://localhost:8080/api/appointments/${id}/finish/`;
		const fetchConfig = {
			method: "PUT",
			body: JSON.stringify({ is_finished: true }),
			headers: {
				"Content-Type": "application/json",
			},
		};

		const response = await fetch(appointmentUrl, fetchConfig);
		if (response.ok) {
			// Remove the finished appointment from the state
			setAppointments(prevAppointments =>
				prevAppointments.map(appointment =>
					appointment.id === id ? { ...appointment, is_finished: true } : appointment
				)
			);
		}
	};

	const cancelAppointment = async (id) => {
		const appointmentUrl = `http://localhost:8080/api/appointments/${id}/cancel/`;
		const fetchConfig = {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
			},
		};

		const response = await fetch(appointmentUrl, fetchConfig);
		if (response.ok) {
			// Remove the canceled appointment from the state
			setAppointments(prevAppointments =>
				prevAppointments.filter(appointment => appointment.id !== id)
			);
		}
	};

	return (
		<>
			<div className="custom-header text-center px-4 py-5 my-1 mt-0">
				<h1 className="display-5">Service Appointments</h1>
			</div>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>Vin</th>
                        <th>Owner</th>
						<th>Date</th>
						<th>Time</th>
						<th>Technician</th>
						<th>Reason</th>
						<th>Vip</th>
					</tr>
				</thead>
				<tbody>
					{appointments.map((appointment) => {
						if (!appointment.is_finished) {
							return (
								<tr className="table-row" key={appointment.id}>
									<td>{appointment.vin}</td>
                                    <td>{appointment.customer}</td>
									<td>
										{new Date(appointment.date_time).toLocaleDateString(
											"en-US"
										)}
									</td>
									<td>
										{new Date(appointment.date_time).toLocaleTimeString([], {
											hour: "2-digit",
											minute: "2-digit",
										})}
									</td>
									<td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
									<td>{appointment.reason}</td>
									<td>{appointment.vip ? "Yes" : "No"}</td>
									<td>
										<button
											onClick={(e) => cancelAppointment(appointment.id)}
                                            style={{ backgroundColor: 'red', color: 'white' }}
                                            className="btn m-2">
                                            Cancel
                                          </button>
                                          <button
                                            onClick={(e) => finishAppointment(appointment.id)}
                                            style={{ backgroundColor: 'green', color: 'white' }}
                                            className="btn">
                                            Finished
										</button>
									</td>
								</tr>
							);
						}
					})}
				</tbody>
			</table>
		</>
	);
};

export default AppointmentsList;
