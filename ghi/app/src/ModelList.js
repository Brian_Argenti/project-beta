import React, {useState, useEffect} from 'react';


function Models(props) {
    const [models, setModels] = useState([]);
    async function getModels() {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok) {
            const { models } = await response.json();
            setModels(models)
           
        } else {
            console.error('An error occured while fetching this data');
        }
        ;
    }

    useEffect(()=> {
        getModels();
    }, []);

    if (models === undefined) {
        return null;
    }

    return (
        <>
        <h1>List of Models</h1>
        <table className="table table-striped">
          <thead>
            <tr className="table-secondary">
              <th scope="col">Model</th>
              <th scope="col">Manufacturer</th>
              <th scope="col">Picture</th>
            </tr>
          </thead>
          <tbody>
            { models.map(model =>{
                return (
                    <tr key={model.href}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td><img src={model.picture_url} /></td>
            </tr>
                )
            })

            }
          </tbody>
        </table>
        </>
    );
}

export default Models;
