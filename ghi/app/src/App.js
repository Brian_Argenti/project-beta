import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerForm from './ManufacturerForm';
import Models from './ModelList';
import ModelsForm from './ModelsForm';
import AutomobileForm from './AutomobileForm';
import ManufacturerList from './ManufacturerList';
import AutomobileList from './AutomobileList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import ServiceHistory from './ServiceHistory';


import SalespeopleList from './SalespeopleList';
import SalespeopleForm from './SalespeopleForm';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import SalesHistory from './SalesHistory';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers/create" element={<ManufacturerForm />}/>
          <Route path="/manufacturers" element={<ManufacturerList />}/>
          <Route path="/appointments/create" element={<AppointmentForm />}/>
          <Route path="/appointments" element={<AppointmentList />}/>
          <Route path="/technicians/create" element={<TechnicianForm />}/>
          <Route path="/technicians" element={<TechnicianList />}/>
          <Route path="/appointments/history" element={<ServiceHistory />}/>
          <Route path="models">
            <Route index element={<Models />} />
            <Route path="create" element={<ModelsForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList />}/>
            <Route path="create" element={<AutomobileForm />} />
          </Route>
          <Route path="salespeople">
            <Route index element={<SalespeopleList />} />
            <Route path="create" element={<SalespeopleForm />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomerList />} />
            <Route path="create" element={<CustomerForm />} />
          </Route>
          <Route path="sales">
            <Route index element={<SalesList />} />
            <Route path="create" element={<SalesForm />} />
            <Route path="history" element={<SalesHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
