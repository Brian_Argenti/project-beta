# CarCar

Team:

* Klein - Automobile Service
* Brian - Automobile Sales

## How to Run this Project:
1. Fork and clone this repository https://gitlab.com/Brian_Argenti/project-beta.git on your preferred directory

2. Make sure you're in the project directory

3. Open Docker Desktop

4. Build the project in Docker with these commands:
-     docker volume create beta-data
-     docker-compose build
-     docker-compose up


5. Wait for all the containers and volumes to setup (3-5 approx.) then type localhost:3000 on your web browser.

## Diagram
![Alt text](image.png)



## Design

CarCar is an app designed to track inventory, service and sales of vehicles.
It's components are divided into three microservices:

 - INVENTORY
    This microservice tracks the vehicles in supply using manufacturer, model, and automoblie type.

 - SERVICE
    This microservice tracks vehicle service appointments for automobiles and their owners.

 - SALES
    This microservice tracks the sale of vehicles to customers by salespeople who make the sale.




## Navigation
| Action            | Method  | URL
|-------|--------|---------
| List manufacturers | GET     | http://localhost:8100/api/manufacturers/|
| Create a manufacturer| POST    | http://localhost:8100/api/manufacturers/|
|Get a specific manufacturer|GET|http://localhost:8100/api/manufacturers/:id/|
|Update a specific manufacturer|PUT|http://localhost:8100/api/manufacturers/:id/|
|Delete a specific manufacturer|DELETE|http://localhost:8100/api/manufacturers/:id/|
|List technicians|GET|http://localhost:8080/api/technicians/|
|Create a technician|POST|http://localhost:8080/api/technicians/|
|Delete a specific technician|DELETE|http://localhost:8080/api/technicians/:id/|
|List appointments|GET|http://localhost:8080/api/appointments/|
|Create an appointment|POST|http://localhost:8080/api/appointments/|
|Delete an appointment|DELETE|http://localhost:8080/api/appointments/:id/|
|Set appointment status to "canceled"|PUT|http://localhost:8080/api/appointments/:id/cancel/|
|Set appointment status to "finished"|PUT|http://localhost:8080/api/appointments/:id/finish/|
|List salespeople|GET|http://localhost:8090/api/salespeople/|
|Create a salesperson|POST|http://localhost:8090/api/salespeople/|
|Delete a specific salesperson|DELETE|http://localhost:8090/api/salespeople/:id/|
|List customers|GET|http://localhost:8090/api/customers/|
|Create a customer|POST|http://localhost:8090/api/customers/|
|Delete a specific customer|DELETE|http://localhost:8090/api/customers/:id/|
|List sales|GET|http://localhost:8090/api/sales/|
|Create a sale|POST|http://localhost:8090/api/sales/|
|Delete a sale|DELETE|http://localhost:8090/api/sales/:id|




## Service microservice
**MODELS AND VALUE OBJECTS**

![Alt text](<services models and VO.png>)


**1. Technician Model**

Attributes:
- first_name: First name of the technician.
- last_name: Last name of the technician.
- employee_id: Unique identifier for the technician.
- get_api_url is used to generate the API endpoint URL for a specific technician.




**2. AutomobileVO (Value Object)**

Attributes:
- vin: Vehicle Identification Number (unique identifier for a vehicle).
- sold: Indicates if the vehicle is sold or not.


*This Value Object is used to fetch automobile data ("vin" and "sold") from the Inventory API and stores it as its own property.




**3. Appointment Model**

Attributes:

- date_time: Date and time for the appointment.
- reason: Reason or purpose for the appointment.
- vin: Vehicle Identification Number associated with the appointment.
- customer: Name of the customer.
- status: Current status of the appointment (default: "pending"). Could be set to cancelled or finished.
- vip: Indicates if the appointment is for a VIP customer or not. VIP status is acquired by customers who bought an automobile from carcar.
- technician: ForeignKey relationship with the Technician model, linking the appointment to a specific technician.
- is_finished: Indicates whether the appointment has been completed or not.
- get_api_urlis used to generate the API endpoint URL for a specific appointment.


Example of how to create a technician (employee ID must be unique. Only numbers are accepted.)

{
  "first_name": "Klein",
  "last_name": "Del Campo",
  "employee_id": 6
}

Example of how to create an Appointment 
{
	"date_time": "2023-12-20T15:30:00Z",
	"technician": "1",
	"reason": "Oil Change",
	"vin": "ASDGTR2000",
	"customer": "Bob"
}

*Use same format and keys for updating an attribute. 


## Sales microservice
Consists of four models which allow user to create, retrieve, view details, and delete three entities: a customer, a salesperson, and a sale.
1. Customer Model
2. Salesperson Model
3. Sale Model
4. Automobile Value Object


## 1. CUSTOMERS
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers | GET | http://localhost:8090/api/customers/
| Create a customer | POST | http://localhost:8090/api/customers/
| Show a specific customer | GET | http://localhost:8090/api/customers/id/


To create a customer, pass data in the format below below into the JSON Body. THis will return a dicitonary wih the id, first name, last name address and phone number of the customer.

entered:
{
			"first_name": "Derek",
			"last_name": "Smith",
			"address": "678 Old Road",
			"phone_number": "5558575309"
		}

returned:
{
	"id": 5,
	"first_name": "Derek",
	"last_name": "Smith",
	"address": "678 Old Road",
	"phone_number": "5558575309"
}

A List of customers will be displayed in the format below:
{
	"customers": [
		{
			"id": 1,
			"first_name": "Brandon",
			"last_name": "Grimes",
			"address": "123 Penny Ln",
			"phone_number": "1234567890"
		},
		{
			"id": 2,
			"first_name": "Cheryl",
			"last_name": "Crow",
			"address": "123 Bird Ct",
			"phone_number": "0123456789"
		},
   ]
}


## 2. SALESPEOPLE

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Salesperson details | GET | http://localhost:8090/api/salesperson/id/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/

To create a salesperon, pass data in the format below into the JSON Body. This will return a dictionary witht the id, first name, last name, and employee id.

entered:
{
	"first_name": "Carl",
	"last_name": "Carlson",
	"employee_id": "11"
}

returned:
{
	"id": 11,
	"first_name": "Carl",
	"last_name": "Carlson",
	"employee_id": "11"
}

A list of salespeople will be returned in the format below:
{
	"salespeople": [
		{
			"id": 1,
			"first_name": "Brian",
			"last_name": "Argenti",
			"employee_id": "1"
		},
		{
			"id": 2,
			"first_name": "Robert",
			"last_name": "Bobby",
			"employee_id": "2"
		}
   ]
}

## 3. SALES

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all sales | GET | http://localhost:8090/api/sales/
| Create a new sale | POST | http://localhost:8090/api/sales/
| Show salesperson's sales | GET | http://localhost:8090/api/sales/id/

To create a sale, pass data in the format below into the JSON body. This will return a dictionary with the sale id, price, and automobile data, salesperson data and customer data.

entered:
{
	"price": "$150,000",
	"automobile": "9876543210987",
	"salesperson": "3",
	"customer": "4"
}

returned:
{
	"id": 6,
	"price": "$150,000",
	"automobile": {
		"id": 8,
		"vin": "9876543210987",
		"sold": true
	},
	"salesperson": {
		"id": 3,
		"first_name": "Jeannie",
		"last_name": "Smith",
		"employee_id": "3"
	},
	"customer": {
		"id": 4,
		"first_name": "Timothy",
		"last_name": "Himothy",
		"address": "466 New Street",
		"phone_number": "8085555309"
	}
}

A list of a sales will be returned in the format below:
{
	"sales": [
		{
			"id": 3,
			"price": "$100,000",
			"automobile": {
				"id": 4,
				"vin": "1C3CC5FB2AN120174",
				"sold": false
			},
			"salesperson": {
				"id": 1,
				"first_name": "Brian",
				"last_name": "Argenti",
				"employee_id": "1"
			},
			"customer": {
				"id": 1,
				"first_name": "Brandon",
				"last_name": "Grimes",
				"address": "123 Penny Ln",
				"phone_number": "1234567890"
			}
		},
		{
			"id": 4,
			"price": "$50,000",
			"automobile": {
				"id": 5,
				"vin": "1234567890",
				"sold": false
			},
			"salesperson": {
				"id": 4,
				"first_name": "Eva",
				"last_name": "Mendez",
				"employee_id": "4"
			},
			"customer": {
				"id": 2,
				"first_name": "Cheryl",
				"last_name": "Crow",
				"address": "123 Bird Ct",
				"phone_number": "0123456789"
			}
      }
   ]
}

A detail of a sale will look like the below data:
{
	"id": 4,
	"price": "$50,000",
	"automobile": {
		"id": 5,
		"sold": false
	},
	"salesperson": {
		"first_name": "Eva",
		"last_name": "Mendez",
		"employee_id": "4"
	},
	"customer": {
		"first_name": "Cheryl",
		"last_name": "Crow",
		"phone_number": "0123456789"
	}
}

![Alt text](<sales model and VO.png>)
